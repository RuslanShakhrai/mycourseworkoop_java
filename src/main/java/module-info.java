module autoinsp {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    requires com.fasterxml.jackson.databind;
    requires javafx.base;
    opens autoinsp to javafx.fxml;
    opens autoinsp.autoowners.controller to javafx.fxml;
    opens autoinsp.MainMenuController to javafx.fxml;
    opens autoinsp.autoowners.model to com.fasterxml.jackson.databind, javafx.base;
    opens autoinsp.utils.model to com.fasterxml.jackson.databind, javafx.base;
    opens autoinsp.utils.controller to com.fasterxml.jackson.databind, javafx.base, javafx.fxml;
    exports autoinsp;
}