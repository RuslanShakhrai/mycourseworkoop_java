package autoinsp.autoowners.controller;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
import autoinsp.FucntionsApp.FunctionsApp;
import autoinsp.MainMenuController.MainMenuController;
import autoinsp.autoowners.model.Adress;
import autoinsp.autoowners.model.AutoOwner;
import autoinsp.autoowners.model.Fio;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javax.swing.*;
public class EditInfoAboutAutoownerController extends FunctionsApp implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField EditFIOField;

    @FXML
    private DatePicker EditDateOfBirthField;

    @FXML
    private TextField EditPassField;

    @FXML
    private TextField EditAdressField;

    @FXML
    private TextField EditIDField;

    @FXML
    private TextField EditAutoMarkField;

    @FXML
    private TextField EditGovNumberField;

    @FXML
    private TextField EditAutoColorField;

    @FXML
    private Button EditConfirmButton;
    @FXML
    private Label IndexLabel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
       autoOwners=getAutoownersData();
        EditConfirmButton.setOnAction(event -> {
    int input= JOptionPane.showConfirmDialog(null,"Вы готовы подтвердить данные?","Подстверждение данных",JOptionPane.YES_NO_CANCEL_OPTION);
   MainMenuController mainMenuController=new MainMenuController();
    if(input==JOptionPane.YES_OPTION) {
    try {
        String indexLabel = IndexLabel.getText();
        int indexlabelPlused = Integer.parseInt(indexLabel);
        int index = indexlabelPlused - 1;
        AutoOwner autoOwner = autoOwners.get(index);
        if (EditFIOField.getText().trim().isEmpty() || EditFIOField == null) {
            Fio fio = autoOwner.getName();
        } else {
            String inputName = EditFIOField.getText();
            Fio fio = new Fio();
            String splitName[] = inputName.split("\\s");
            fio.setFirstName(splitName[0]);
            fio.setSecondName(splitName[1]);
            fio.setLastName(splitName[2]);
            autoOwner.setName(fio);
        }
        if (EditPassField.getText().trim().isEmpty() || EditPassField == null) {
            String pass = autoOwner.getPass();
        } else {
            String pass = EditPassField.getText();
            autoOwner.setPass(pass);
        }
        if (EditGovNumberField.getText().trim().isEmpty() || EditGovNumberField == null) {
            String govNumber = autoOwner.getGovNumber();
        } else {
            String govnumber = EditGovNumberField.getText();
            autoOwner.setGovNumber(govnumber);

        }
        if (EditAdressField.getText().trim().isEmpty() || EditAdressField == null) {
            Adress adress = autoOwner.getAdress();
        } else {
            Adress adress = new Adress();
            String inputAdress = EditAdressField.getText();
            String[] splitter = inputAdress.split("\\s");
            adress.setCity(splitter[0]);
            adress.setStreet(splitter[1]);
            adress.setZip(splitter[2]);
            autoOwner.setAdress(adress);
        }
        Date dateOfBirth = null;
        TextField editableDate = EditDateOfBirthField.getEditor();
        if (EditDateOfBirthField.getValue() != null) {
            LocalDate localDate = EditDateOfBirthField.getValue();
            dateOfBirth = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
            simpleDateFormat.format(dateOfBirth);
            autoOwner.setDateOfBirth(dateOfBirth);
        } else if (EditDateOfBirthField.getValue() != null || editableDate == null) {
            Date date = autoOwner.getDateOfBirth();
        }
        else {
            try {
                String date = editableDate.getText();
                dateOfBirth = new SimpleDateFormat("dd.MM.yyyy").parse(date);
                autoOwner.setDateOfBirth(dateOfBirth);
            }
            catch (ParseException ex)
            {
                Date ownerDateOfBirth=autoOwner.getDateOfBirth();
            }
        }
        if (EditIDField.getText().trim().isEmpty() || EditIDField == null) {
            int id = autoOwner.getId();
        } else {
            String inputId = EditIDField.getText();
            int id = Integer.parseInt(inputId);
            autoOwner.setId(id);
        }
        if (EditAutoMarkField.getText().trim().isEmpty() || EditAutoMarkField == null) {
            String autoMark = autoOwner.getAutoMark();
        } else {
            String autoMark = EditAutoMarkField.getText();
            autoOwner.setAutoMark(autoMark);
        }
        if (EditAutoColorField.getText().trim().isEmpty() || EditAutoColorField == null) {
            String autoColor = autoOwner.getAutoColor();
        } else {
            String autocolor = EditAutoColorField.getText();
            autoOwner.setAutoColor(autocolor);
        }
        JOptionPane.showMessageDialog(null, "Изменения произошли успешно", "Success", JOptionPane.INFORMATION_MESSAGE);
        EditInfoAboutAutoownerController editInfoAboutAutoownerController=new EditInfoAboutAutoownerController();
        editInfoAboutAutoownerController.saveInfo("Autoowners.json",autoOwners);
        mainMenuController.moveMainMenu(EditConfirmButton);
    }
    catch (IndexOutOfBoundsException ex) {
        JOptionPane.showMessageDialog(null, "Введите ФИО||адрес полностью", "Warning", JOptionPane.WARNING_MESSAGE);

    } catch (NumberFormatException ex) {
        JOptionPane.showMessageDialog(null, "ID должен быть целым числом", "Warning", JOptionPane.WARNING_MESSAGE);
    }
}
else if(input==JOptionPane.CANCEL_OPTION)
{
    mainMenuController.moveMainMenu(EditConfirmButton);
}
});
    }
    public void setIndexLabel(int index)
    {
        int passingLabelIndex=index+1;
        String labelIndex=String.valueOf(passingLabelIndex);
        IndexLabel.setText(labelIndex);
    }
}
