package autoinsp.autoowners.controller;
import autoinsp.FucntionsApp.FunctionsApp;
import autoinsp.MainMenuController.MainMenuController;
import autoinsp.autoowners.model.Adress;
import autoinsp.autoowners.model.AutoOwner;
import autoinsp.autoowners.model.Fio;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javax.swing.*;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
public class AddOwnerController implements Initializable {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField FIOField;

    @FXML
    private TextField PassField;

    @FXML
    private TextField AutoMarkField;

    @FXML
    private TextField GovNumberField;

    @FXML
    private TextField AutoColorField;

    @FXML
    private TextField AdressField;

    @FXML
    private Button ConfirmButton;

    @FXML
    private DatePicker DateOfBirthField;

    @FXML
    private TextField IDField;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ConfirmButton.setOnAction(event -> {
            int input = JOptionPane.showConfirmDialog(null,
                    "Вы готовы подтвердить данные?", "Подтверждение данных", JOptionPane.YES_NO_CANCEL_OPTION);
           MainMenuController mainMenuController=new MainMenuController();
            if (input ==JOptionPane.YES_OPTION) {
                try {
                    String inputName = FIOField.getText();
                    Fio fio=new Fio();
                    String splitName[]=inputName.split("\\s");
                    fio.setFirstName(splitName[0]);
                    fio.setSecondName(splitName[1]);
                    fio.setLastName(splitName[2]);
                    TextField editableDate=DateOfBirthField.getEditor();
                    String date=editableDate.getText();
                    Date dateOfBirth= null;
                        dateOfBirth = new SimpleDateFormat("dd.mm.yyyy").parse(date);
                    if(DateOfBirthField.getValue()!=null)
                        {
                    LocalDate localDate=DateOfBirthField.getValue();
                    dateOfBirth=Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd.mm.yyyy");
                    simpleDateFormat.format(dateOfBirth);
                        }
                    Adress adress = new Adress();
                    String inputAdress = AdressField.getText();
                    String[] splitter = inputAdress.split("\\s");
                    adress.setCity(splitter[0]);
                    adress.setStreet(splitter[1]);
                    adress.setZip(splitter[2]);
                    String pass = PassField.getText();
                    String inputId=IDField.getText();
                    int id=Integer.parseInt(inputId);
                    String autoMark = AutoMarkField.getText();
                    String govNumber = GovNumberField.getText();
                    String autoColor = AutoColorField.getText();
                    AutoOwner autoOwner = new AutoOwner(fio,dateOfBirth,adress,pass,id,autoMark,govNumber,autoColor);
                    JOptionPane.showMessageDialog(null, autoOwner,"Добавленный автовладелец" ,JOptionPane.INFORMATION_MESSAGE);
                    FunctionsApp serializator=new FunctionsApp();
                    serializator.saveAutoownertoFile(autoOwner);
                    mainMenuController.moveMainMenu(ConfirmButton);
                }
                catch (IndexOutOfBoundsException ex)
                {
                    JOptionPane.showMessageDialog(null,"Введите ФИО/ Адрес полностью","Warning",JOptionPane.WARNING_MESSAGE);
                }
                catch (NumberFormatException ex)
                {
                    JOptionPane.showMessageDialog(null,"ID должен быть числом","Error",JOptionPane.ERROR_MESSAGE);
                }
                catch (ParseException ex)
                {
                    JOptionPane.showMessageDialog(null,"Введите дату в указанном формате","Warning",JOptionPane.WARNING_MESSAGE);
                }
            }
           else if(input==JOptionPane.CANCEL_OPTION)
            {
                mainMenuController.moveMainMenu(ConfirmButton);
            }
        });
    }
}




