package autoinsp.autoowners.controller;
import autoinsp.FucntionsApp.FunctionsApp;
import autoinsp.MainMenuController.MainMenuController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javax.swing.*;
import java.net.URL;
import java.util.*;
public class RemoveAutoownerController extends FunctionsApp implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button DeleteConfirmButton;

    @FXML
    private TextField RemoveAutoownerField;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        DeleteConfirmButton.setOnAction(event -> {
            autoOwners=getAutoownersData();
            int input = JOptionPane.showConfirmDialog(null,"Вы готовы подтвердить данные?", "Подтверждение данных",JOptionPane.YES_NO_CANCEL_OPTION);
            MainMenuController mainMenuController=new MainMenuController();
            if(input==JOptionPane.YES_OPTION)
            {
                try {
                    String inputId = RemoveAutoownerField.getText();
                    int id = Integer.parseInt(inputId);
                    int indexID = id - 1;
                    FunctionsApp functionsApp = new FunctionsApp();
                    JOptionPane.showMessageDialog(null,autoOwners.get(indexID),"Удаленный автовладелец", JOptionPane.INFORMATION_MESSAGE);
                    functionsApp.removeAutoowner(indexID);
                    mainMenuController.moveMainMenu(DeleteConfirmButton);
                }
                catch (NumberFormatException ex)
                {
                    JOptionPane.showMessageDialog(null,"Index должен быть целым числом","Error",JOptionPane.ERROR_MESSAGE);
                }
                catch (IndexOutOfBoundsException ex)
                {
                    JOptionPane.showMessageDialog(null,"Неверно введенный Index","Error",JOptionPane.ERROR_MESSAGE);
                }

            }
             else if (input==JOptionPane.CANCEL_OPTION)
            {
                mainMenuController.moveMainMenu(DeleteConfirmButton);
            }
        });
    }
}
