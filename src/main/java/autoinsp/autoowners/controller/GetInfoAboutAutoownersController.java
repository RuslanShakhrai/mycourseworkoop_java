package autoinsp.autoowners.controller;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import autoinsp.FucntionsApp.FunctionsApp;
import autoinsp.MainMenuController.MainMenuController;
import autoinsp.autoowners.model.Adress;
import autoinsp.autoowners.model.AutoOwner;
import autoinsp.autoowners.model.Fio;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.*;

public class GetInfoAboutAutoownersController extends FunctionsApp implements Initializable  {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView<AutoOwner> tableAutoowners;

    @FXML
    private TableColumn<AutoOwner, Integer> idColumn;

    @FXML
    private TableColumn<AutoOwner, Fio> FioColumnn;

    @FXML
    private TableColumn<AutoOwner, Date> DateOfBirthColumn;

    @FXML
    private TableColumn<AutoOwner, Adress> AdressColumn;

    @FXML
    private TableColumn<AutoOwner,String > PassColumn;

    @FXML
    private TableColumn<AutoOwner, String> AutoMarkColumn;

    @FXML
    private TableColumn<AutoOwner,String > GovNumberColumn;

    @FXML
    private TableColumn<AutoOwner, String> AutoColorColumn;
    @FXML
    private Button BackButton;
    @FXML
    private TextField SearchField;
    @FXML
    private Button GetAutoownersIndex;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {


        idColumn.setCellValueFactory(new PropertyValueFactory<AutoOwner, Integer>("id"));
        FioColumnn.setCellValueFactory(new PropertyValueFactory<AutoOwner, Fio>("name"));
        DateOfBirthColumn.setCellValueFactory(new PropertyValueFactory<AutoOwner,Date>("dateOfBirth"));
        DateOfBirthColumn.setCellFactory(column -> {
            TableCell<AutoOwner, Date> cell = new TableCell<AutoOwner, Date>() {
                private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                @Override
                protected void updateItem(Date item, boolean empty) {
                    super.updateItem(item, empty);
                    if(empty) {
                        setText(null);
                    }
                    else {
                        setText(format.format(item));
                    }
                }
            };
            return cell;
        });


        AdressColumn.setCellValueFactory(new PropertyValueFactory<AutoOwner, Adress>("adress"));
        PassColumn.setCellValueFactory(new PropertyValueFactory<AutoOwner, String>("pass"));
        AutoMarkColumn.setCellValueFactory(new PropertyValueFactory<AutoOwner, String>("autoMark"));
        GovNumberColumn.setCellValueFactory(new PropertyValueFactory<AutoOwner, String>("govNumber"));
        AutoColorColumn.setCellValueFactory(new PropertyValueFactory<AutoOwner, String>("autoColor"));
        autoOwners=getAutoownersData();
            ObservableList<AutoOwner> autoOwnersData = FXCollections.observableArrayList();
            autoOwnersData.addAll(autoOwners);
        tableAutoowners.setItems(autoOwnersData);
        FilteredList<AutoOwner>filteredListData=new FilteredList<>(autoOwnersData,b->true);
        SearchField.textProperty().addListener((observable,oldvalue,newValue)->{
            filteredListData.setPredicate(autoOwner -> {
                String space=" ";
                String firstname=autoOwner.getName().getFirstName();
                String secondname=autoOwner.getName().getSecondName();
                String lastname=autoOwner.getName().getLastName();
                String Fio=firstname+space+secondname+space+lastname;
                String city=autoOwner.getAdress().getCity();
                String street=autoOwner.getAdress().getStreet();
                String zip=autoOwner.getAdress().getZip();
                String adress=city+space+street+space+zip;
                DateFormat dateFormat=new SimpleDateFormat("dd.MM.yyyy");
                Date dateOfBirth=autoOwner.getDateOfBirth();
                String dateOfBirthString=dateFormat.format(dateOfBirth);
                if(newValue==null||newValue.isEmpty())
                {
                    return true;
                }
                String lowerCaseFilter=newValue.toLowerCase();
                if((Fio.toLowerCase().indexOf(lowerCaseFilter))!=-1)
                {
                    return true;
                }
                else if(adress.toLowerCase().indexOf(lowerCaseFilter)!=-1)
                {
                    return true;
                }
                else if(autoOwner.getAutoColor().toLowerCase().indexOf(lowerCaseFilter)!=-1)
                {
                    return true;
                }
                else if(autoOwner.getAutoMark().toLowerCase().indexOf(lowerCaseFilter)!=-1)
                {
                    return true;
                }
                else if(autoOwner.getGovNumber().toLowerCase().indexOf(lowerCaseFilter)!=-1)
                {
                    return true;
                }
                else if(autoOwner.getPass().toLowerCase().indexOf(lowerCaseFilter)!=-1)
                {
                    return true;
                }
                else if(String.valueOf(autoOwner.getId()).indexOf(lowerCaseFilter)!=-1)
                {
                    return true;
                }
                else if(dateOfBirthString.toLowerCase().indexOf(lowerCaseFilter)!=-1)
                {
                    return true;
                }
                else
                return false;
            });
        });
        SortedList<AutoOwner>sortedList=new SortedList<>(filteredListData);
        sortedList.comparatorProperty().bind(tableAutoowners.comparatorProperty());
        tableAutoowners.setItems(sortedList);
        BackButton.setOnAction(event -> {
            int input= JOptionPane.showConfirmDialog(null,"Вы действительно хотите вернуться в главное меню? ","Сделайте выбор",JOptionPane.YES_NO_OPTION);
            if(input==JOptionPane.YES_OPTION)
            {
                MainMenuController mainMenuController=new MainMenuController();
                mainMenuController.moveMainMenu(BackButton);
            }
        });
        GetAutoownersIndex.setOnAction(event -> {
            AutoOwner autoOwner=tableAutoowners.getSelectionModel().getSelectedItem();
            int selectedIndex=autoOwners.indexOf(autoOwner);
            if(selectedIndex==-1)
            {
                JOptionPane.showMessageDialog(null,"Вы не выбрали автовладельца ","Warning ",JOptionPane.WARNING_MESSAGE);
            }
            else {
                int index=selectedIndex+1;
                JOptionPane.showMessageDialog(null,"Индекс выделенного автовладельца: "+index,"Индекс ",JOptionPane.INFORMATION_MESSAGE);
            }
        });
    }
    }


