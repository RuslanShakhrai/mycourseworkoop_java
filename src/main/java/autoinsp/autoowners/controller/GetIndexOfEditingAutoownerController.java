package autoinsp.autoowners.controller;
import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;
import autoinsp.FucntionsApp.FunctionsApp;
import autoinsp.MainMenuController.MainMenuController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javax.swing.*;
public class GetIndexOfEditingAutoownerController extends FunctionsApp implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button IndexConfirmButton;

    @FXML
    private TextField EditingIdnexField;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        IndexConfirmButton.setOnAction(event -> {
            int input=JOptionPane.showConfirmDialog(null,"Вы готовы подтвердить индекс?","Подтверждение данных",JOptionPane.YES_NO_CANCEL_OPTION);
            if(input==JOptionPane.YES_OPTION) {
                try {
                    autoOwners=getAutoownersData();
                    String inputIdnex = EditingIdnexField.getText();
                    int inputIndex = Integer.parseInt(inputIdnex);
                    int index=inputIndex-1;
                    autoOwners.get(index);
                    FXMLLoader loader=new FXMLLoader();
                    IndexConfirmButton.getScene().getWindow().hide();
                    loader.setLocation(getClass().getResource("/view/EditInfoAboutAutoowner.fxml"));
                    try {
                        loader.load();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                    Parent root=loader.getRoot();
                    Stage stage=new Stage();
                    EditInfoAboutAutoownerController editInfoAboutAutoownerController=loader.getController();
                    editInfoAboutAutoownerController.setIndexLabel(index);
                    stage.setScene(new Scene(root));
                    Image ico = null;
                    try {
                        ico = new Image(new FileInputStream("src/main/resources/images/icon.png"));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    stage.getIcons().add(ico);
                    stage.setTitle("АИС ГИБДД ЛНР");
                    stage.show();
                }
                catch (IndexOutOfBoundsException ex) {
                    JOptionPane.showMessageDialog(null, "Выбранный индекс не существует", "Error", JOptionPane.ERROR_MESSAGE);
                }
                catch (NumberFormatException ex)
                {
                    JOptionPane.showMessageDialog(null,"Index должен быть целым числом","Warning",JOptionPane.WARNING_MESSAGE);
                }
            }
                else if(input==JOptionPane.CANCEL_OPTION)
                {
                    MainMenuController mainMenuController=new MainMenuController();
                    mainMenuController.moveMainMenu(IndexConfirmButton);
                }
        });
    }
}
