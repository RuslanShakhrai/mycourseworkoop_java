package autoinsp.autoowners.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Person implements Serializable {
    private Fio name;// ФИО человека
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd.MM.yyyy")
    private Date dateOfBirth;// дата рождения человека
    private Adress adress;//адрес человека
    private String pass;// серия и номер пасспорта
    public Person()
    {}
    public Person(Fio name,Date  dateOfBirth,Adress adress,String pass)
    {
        this.name=name;
        this.dateOfBirth=dateOfBirth;
        this.adress=adress;
        this.pass=pass;
    }
    public Fio getName()
    {
        return name;
    }
    public Date getDateOfBirth()
    {
       return dateOfBirth;
    }
    public String getPass()
    {
        return pass;
    }
    public Adress getAdress()
    {
        return adress;
    }
    public void setName(Fio name)
    {
        this.name=name;
    }
    public void setDateOfBirth(Date dateOfBirth)
    {
        this.dateOfBirth=dateOfBirth;
    }
    public void setAdress(Adress adress)
    {
        this.adress=adress;
    }
    public void setPass(String pass)
    {
        this.pass=pass;
    }
}


