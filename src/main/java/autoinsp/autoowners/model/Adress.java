package autoinsp.autoowners.model;

public class Adress {
    private  String city;
    private String street;
    private String zip;// номер дома/квартиры
    public Adress(){}
    public Adress(String city,String street,String zip)
    {
        this.street=street;
        this.city=city;
        this.zip=zip;
    }
    public String getStreet()
    {
        return street;
    }
    public String getCity()
    {
        return city;
    }
    public String getZip()
    {
        return zip;
    }
    public void setStreet(String street)
    {
        this.street=street;
    }
    public void setCity(String city)
    {
        this.city=city;
    }
    public void setZip(String zip)
    {
        this.zip=zip;
    }
    public String toString()
    {
        String adress;
        adress= "Город: "+this.city+"; улица: "+this.street+ "; номер дома/номер квартиры: "+this.zip;
        return adress;
    }
}
