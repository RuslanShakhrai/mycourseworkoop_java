package autoinsp.autoowners.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

public class AutoOwner extends Person implements Serializable {
    private int id;// уникальный идентификатор автовладельца
    private String autoMark;// марка авто
    private String govNumber;//государственный номер
    private String autoColor;// цвет автомобиля
    //Метод вывода информации о автовладельце
    public String toString() {
        String s=new String();

        s=" ID "+this.id+"\n ФИО: "+this.getName()+"\n Дата рождения: "+this.getDateOfBirth()+"\n Адрес: "+this.getAdress()+ "\n Серия и номер паспорта: "+this.getPass()+ "\n Марка авто: "+this.autoMark+ "\n Номер автомобиля: "+this.govNumber+ "\n Цвет автомобиля "+this.autoColor;
        return s;
    }
public AutoOwner(){};
    public AutoOwner(Fio name, Date dateOfBirth, Adress adress, String pass, int id, String autoMark, String govNumber, String autoColor)
    {

        super(name, dateOfBirth, adress,pass);
        this.id=id;
        this.autoMark=autoMark;
        this.govNumber=govNumber;
        this.autoColor=autoColor;
    }
    public int getId()
    {
        return id;
    }
    public String getAutoMark()
    {
        return autoMark;
    }
    public String getGovNumber()
    {
        return govNumber;
    }
    public String getAutoColor()
    {
        return autoColor;
    }
    public void setAutoMark(String autoMark)
    {
        this.autoMark=autoMark;
    }
    public void setGovNumber(String govNumber)
    {
        this.govNumber=govNumber;
    }
    public void setAutoColor(String autoColor)
    {
        this.autoColor=autoColor;
    }
    public void setId(int id)
    {
        this.id=id;
    }
}


