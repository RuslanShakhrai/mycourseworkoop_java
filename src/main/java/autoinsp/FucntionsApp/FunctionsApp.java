package autoinsp.FucntionsApp;
import autoinsp.autoowners.model.AutoOwner;
import autoinsp.utils.model.Fine;
import autoinsp.utils.model.ViolationLog;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import javax.swing.*;
import java.io.*;
import java.util.*;
public class FunctionsApp {
    protected LinkedList<AutoOwner> autoOwners = new LinkedList<>();
    protected LinkedList<ViolationLog> violationLogs = new LinkedList<>();
    protected LinkedList<Fine> finesList = new LinkedList<>();
    boolean flag = false;
    public boolean saveAutoownertoFile(AutoOwner autoOwner) {
        String path="Autoowners.json";
        FunctionsApp functionsApp=new FunctionsApp();
        File file = new File(path);
        if (file.length() == 0) {
            autoOwners.add(autoOwner);
            functionsApp.saveInfo(path,autoOwners);
            return flag;
        } else {
            autoOwners=getAutoownersData();
            autoOwners.add(autoOwner);
            functionsApp.saveInfo(path,autoOwners);
            return flag;
        }
    }

    public boolean removeAutoowner(int inputId) {
        String path="Autoowners.json";
        FunctionsApp functionsApp=new FunctionsApp();
        autoOwners=getAutoownersData();
        autoOwners.remove(inputId);
        flag = true;
        functionsApp.saveInfo(path,autoOwners);
        return flag;
    }

    public boolean saveToFileViolantionLog(ViolationLog violationLog) {
        String path="ViolantionsLog.json";
        FunctionsApp functionsApp=new FunctionsApp();
        File file = new File(path);
        if (file.length() == 0) {
            violationLogs.add(violationLog);
            functionsApp.saveInfo(path,violationLogs);
            return flag;
        } else {
            violationLogs=getViolationLogData();
            violationLogs.add(violationLog);
            functionsApp.saveInfo(path,violationLogs);
            return flag;
        }
    }

    public boolean saveFinesToFile(Fine fine) {
        String path="Fines.json";
        FunctionsApp functionsApp=new FunctionsApp();
        File file = new File(path);
        if (file.length() == 0) {
            finesList.add(fine);
          functionsApp.saveInfo(path,finesList);
            return flag;
        } else {
            finesList=getFinesData();
            finesList.add(fine);
            functionsApp.saveInfo(path,finesList);
            return flag;
        }
    }
    public  boolean saveInfo(String path, LinkedList linkedList) {
        boolean flag = false;
        File file = new File(path);
        ObjectMapper objectMapper = new ObjectMapper();
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            ObjectWriter writer = objectMapper.writer(new DefaultPrettyPrinter());
            writer.writeValue(fileOutputStream, linkedList);
            flag = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return flag;
    }

    public static LinkedList<AutoOwner> getAutoownersData()
    {
        String filePath="Autoowners.json";
        LinkedList<AutoOwner>linkedList=new LinkedList<>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            File file = new File(filePath);
            linkedList = mapper.readValue(file, new TypeReference<LinkedList<AutoOwner>>(){});
        }
        catch (MismatchedInputException ex)
        {
            JOptionPane.showMessageDialog(null,"Данные отсутствуют ","Error",JOptionPane.ERROR_MESSAGE);

        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        return linkedList;
    }
    public static LinkedList<Fine> getFinesData()
    {
        String filePath="Fines.json";
        LinkedList<Fine>linkedList=new LinkedList<>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            File file = new File(filePath);
            linkedList = mapper.readValue(file, new TypeReference<LinkedList<Fine>>(){});
        }
        catch (MismatchedInputException ex)
        {
            JOptionPane.showMessageDialog(null,"Данные отсутствуют ","Error",JOptionPane.ERROR_MESSAGE);

        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        return linkedList;
    }
    public static LinkedList<ViolationLog> getViolationLogData()
    {
        String filePath="ViolantionsLog.json";
        LinkedList<ViolationLog>linkedList=new LinkedList<>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            File file = new File(filePath);
            linkedList = mapper.readValue(file, new TypeReference<LinkedList<ViolationLog>>(){});
        }
        catch (MismatchedInputException ex)
        {
            JOptionPane.showMessageDialog(null,"Данные отсутствуют ","Error",JOptionPane.ERROR_MESSAGE);

        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        return linkedList;
    }
}














