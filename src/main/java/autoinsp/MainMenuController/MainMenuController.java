package autoinsp.MainMenuController;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javax.swing.*;
public class MainMenuController implements Initializable {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button ExitButton;

    @FXML
    private Button AddNewOwnerButton;

    @FXML
    private Button DeleteOwnerButton;

    @FXML
    private Button GetInfoOwnersButton;

    @FXML
    private Button EditInfoOwnersButton;
    @FXML
    private Button AboutButton;
    @FXML
    private Button FindMostUnDisciplinedOwnerButton;
    @FXML
    private Button AddInfoAboutViolationsButton;
    @FXML
    private Button GetViolationLogButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        MainMenuController mainMenuController=new MainMenuController();
        ExitButton.setOnAction(event -> {
            int input = JOptionPane.showConfirmDialog(null,
                    "Вы действительно хотите выйти?", "Сделайте выбор", JOptionPane.YES_NO_OPTION);
            if (input == JOptionPane.YES_OPTION) {
                System.exit(0);
            }
        });
        AboutButton.setOnAction(event -> {
            JOptionPane.showMessageDialog(null, "Данная программа разрабатывается в тестовом режиме для ГИБДД ЛНР", "О программе",JOptionPane.INFORMATION_MESSAGE);
        });
        AddNewOwnerButton.setOnAction(event -> {
            String path="/view/AddNewAutoowner.fxml";
            String title="Добавить нового автовладельца";
            mainMenuController.moveForm(AddNewOwnerButton,path,title);
        });
        DeleteOwnerButton.setOnAction(event ->
        {        String path="/view/RemoveAutoowner.fxml";
                 String title="Удалить автовладельца";
            mainMenuController.moveForm(DeleteOwnerButton,path,title);
        });
        GetInfoOwnersButton.setOnAction(event -> {
            String path="/view/GetInfoAboutAutoowners.fxml";
            String title="Информация о автовладельцах";
            mainMenuController.moveForm(GetInfoOwnersButton,path,title);
        });
        EditInfoOwnersButton.setOnAction(event -> {
            String path="/view/GetIndexOfEditingAutoowner.fxml";
            String title="Индекс редактируемого автовладельца";
            mainMenuController.moveForm(EditInfoOwnersButton,path,title);
        });
        GetViolationLogButton.setOnAction(event -> {
            String path="/view/GetInfoAboutViolationLog.fxml";
            String title="Журнал нарушений";
            mainMenuController.moveForm(GetViolationLogButton,path,title);
        });
        AddInfoAboutViolationsButton.setOnAction(event -> {
            String path="/view/AddNewInfoAboutViolants.fxml";
            String title="Добавление новых правонарушителей";
            mainMenuController.moveForm(AddInfoAboutViolationsButton,path,title);
        });
        FindMostUnDisciplinedOwnerButton.setOnAction(event -> {
            String path="/view/FindMostUndisciplinedViolator.fxml";
            String title="Самый недисциплинированный нарушитель";
            mainMenuController.moveForm(FindMostUnDisciplinedOwnerButton,path,title);
        });
    }
    public boolean moveForm(Button button,String path,String title)
    {
        boolean flag=false;
        button.getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(path));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        Image ico = null;
        try {
            ico = new Image(new FileInputStream("src/main/resources/images/icon.png"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        stage.getIcons().add(ico);
        stage.setTitle(title);
        stage.show();
        flag=true;
        return flag;
    }
    public boolean moveMainMenu(Button button)
    {
        boolean flag=false;
        String path="/view/MainMenu.fxml";
        String title="АИС ГИБДД ЛНР";
        moveForm(button,path,title);
        flag=true;
        return flag;
    }
}

