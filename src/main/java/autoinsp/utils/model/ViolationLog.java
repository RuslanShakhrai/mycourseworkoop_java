package autoinsp.utils.model;
import autoinsp.autoowners.model.Adress;
import autoinsp.autoowners.model.Fio;
import autoinsp.autoowners.model.Person;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.*;

public class ViolationLog  extends Person implements Serializable {
    private int id;//номер нарушения
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd.MM.yyyy")
    private Date date;//Дата нарушения
    private Fine type;//тип нарушения
    public ViolationLog()
    {};
    public ViolationLog(Fio name, Date dateOfBirth, Adress adress, String pass, int id, Date date, Fine type ) {
        super(name, dateOfBirth, adress, pass);
        this.id = id;
        this.date = date;
        this.type=type;
    }
    public int getId()
    {
        return id;
    }
    public Fine getType()
    {
        return type;
    }
    public void setType(Fine type)
    {
        this.type=type;
    }

    public Date getDate()
    {
        return date;
    }

    public void setId(int id)
    {
        this.id=id;
    }
    public void setDate(Date date)
    {
        this.date=date;
    }


    public String toString()
    {
        String s=new String();
        s="\nId:"+this.id+"\nФИО: "+this.getName()+"\nДата рождения: "+this.getDateOfBirth()+"\nАдрес: "+this.getAdress()+"\nПаспортные данные: "+this.getPass()+"\nДата нарушения: "+date+"\nТип нарушения(й): "+this.getType();
    return s;
    }
        }


