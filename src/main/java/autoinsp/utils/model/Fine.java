package autoinsp.utils.model;
import java.io.Serializable;
public class Fine implements Serializable {
    private String typeOfFine;
    private Double sumOFFine;
    public Fine()
    {};
    public Fine(String typeOfFine, Double sumOFFine)
    {
        this.typeOfFine=typeOfFine;
        this.sumOFFine=sumOFFine;
    }
    public String getTypeOfFine()
    {
        return typeOfFine;
    }
    public Double getSumOFFine()
    {
        return sumOFFine;
    }
    public void setTypeOfFine(String typeOfFine)
    {
        this.typeOfFine=typeOfFine;
    }
    public void setSumOFFine(Double sumOFFine)
    {
        this.sumOFFine=sumOFFine;
    }
    public String toString() {
        return typeOfFine;
    }
}
