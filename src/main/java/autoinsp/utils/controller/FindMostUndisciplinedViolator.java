package autoinsp.utils.controller;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import autoinsp.FucntionsApp.FunctionsApp;
import autoinsp.MainMenuController.MainMenuController;
import autoinsp.utils.model.ViolationLog;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javax.swing.*;

public class FindMostUndisciplinedViolator extends FunctionsApp implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private AnchorPane DateOfViolant;

    @FXML
    private TextField FIOField;

    @FXML
    private DatePicker DateOfBirthField;

    @FXML
    private TextField PassField;

    @FXML
    private TextField AdressField;

    @FXML
    private TextField IDField;

    @FXML
    private DatePicker DateOfViolantsPicker;

    @FXML
    private Button BackButton;

    @FXML
    private TextField TypeOfViolant;

    @FXML
    private TextField SumOFViolants;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        violationLogs=getViolationLogData();
        double temp=0;
        int max=0;
        for(int i=0;i<violationLogs.size();i++)
        {
            if(violationLogs.get(i).getType().getSumOFFine()>temp)
            {
                temp=violationLogs.get(i).getType().getSumOFFine();
                max=i;
            }
        }
        ViolationLog maxViolator=violationLogs.get(max);
        FIOField.setText(maxViolator.getName().getFirstName()+" "+maxViolator.getName().getSecondName()+" "+maxViolator.getName().getLastName());
        Date date=maxViolator.getDateOfBirth();
        LocalDate localDate = Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        DateOfBirthField.setValue(localDate);
        PassField.setText(maxViolator.getPass());
        AdressField.setText(maxViolator.getAdress().getCity()+" "+maxViolator.getAdress().getStreet()+" "+maxViolator.getAdress().getZip());
        String id=Integer.toString(maxViolator.getId());
        IDField.setText(id);
        Date dateOfViolant=maxViolator.getDate();
        LocalDate localDateViolant=Instant.ofEpochMilli(dateOfViolant.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        DateOfViolantsPicker.setValue(localDateViolant);
        TypeOfViolant.setText(maxViolator.getType().getTypeOfFine());
        String sumOfFine=Double.toString(maxViolator.getType().getSumOFFine());
        SumOFViolants.setText(sumOfFine);
        BackButton.setOnAction(event -> {
            MainMenuController mainMenuController=new MainMenuController();
            int input = JOptionPane.showConfirmDialog(null,
                    "Вы действительно хотите вернуться в главное меню?", "Подтверждение выброра", JOptionPane.YES_NO_OPTION);
            if (input ==JOptionPane.YES_OPTION) {
                mainMenuController.moveMainMenu(BackButton);
            }
        });
    }

    }

