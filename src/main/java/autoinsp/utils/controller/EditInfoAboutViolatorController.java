package autoinsp.utils.controller;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
import autoinsp.FucntionsApp.FunctionsApp;
import autoinsp.MainMenuController.MainMenuController;
import autoinsp.autoowners.model.Adress;
import autoinsp.autoowners.model.Fio;
import autoinsp.utils.model.Fine;
import autoinsp.utils.model.ViolationLog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javax.swing.*;

public class EditInfoAboutViolatorController extends FunctionsApp implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label IndexLabel;

    @FXML
    private TextField EditFIOField;

    @FXML
    private DatePicker EditDateOfBirthField;

    @FXML
    private TextField EditPassField;

    @FXML
    private TextField EditAdressField;

    @FXML
    private TextField EditIDField;

    @FXML
    private Button EditConfirmButton;

    @FXML
    private DatePicker EditDateOfVioalnt;

    @FXML
    private ComboBox<Fine> EditTypeOFViolation;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
       violationLogs=getViolationLogData();
       finesList=getFinesData();
        ObservableList<Fine> finesDataList= FXCollections.observableArrayList();
        finesDataList.addAll(finesList);
        EditTypeOFViolation.setItems(finesDataList);
        EditConfirmButton.setOnAction(event -> {
            int input= JOptionPane.showConfirmDialog(null,"Вы готовы подтвердить данные?","Подтверждение данных",JOptionPane.YES_NO_CANCEL_OPTION);
            MainMenuController mainMenuController=new MainMenuController();
            if(input== JOptionPane.YES_OPTION) {
                try {
                    String inputIndex = IndexLabel.getText();
                    int indexPlused = Integer.parseInt(inputIndex);
                    int index = indexPlused - 1;
                    ViolationLog violationLog = violationLogs.get(index);
                    if (EditFIOField.getText().trim().isEmpty() || EditFIOField == null) {
                        Fio fio = violationLog.getName();
                    } else {
                        String inputName = EditFIOField.getText();
                        Fio fio = new Fio();
                        String splitName[] = inputName.split("\\s");
                        fio.setFirstName(splitName[0]);
                        fio.setSecondName(splitName[1]);
                        fio.setLastName(splitName[2]);
                        violationLog.setName(fio);
                    }
                    if (EditAdressField.getText().trim().isEmpty() || EditAdressField == null) {
                        Adress adress = violationLog.getAdress();
                    } else {
                        Adress adress = new Adress();
                        String inputAdress = EditAdressField.getText();
                        String[] splitter = inputAdress.split("\\s");
                        adress.setCity(splitter[0]);
                        adress.setStreet(splitter[1]);
                        adress.setZip(splitter[2]);
                        violationLog.setAdress(adress);
                    }
                    if (EditPassField.getText().trim().isEmpty() || EditPassField == null) {
                        String pass = violationLog.getPass();
                    } else {
                        String pass = EditPassField.getText();
                        violationLog.setPass(pass);
                    }
                    Date dateOfBirth = null;
                    TextField editableDate = EditDateOfBirthField.getEditor();
                    if (EditDateOfBirthField.getValue() != null) {
                        LocalDate localDate = EditDateOfBirthField.getValue();
                        dateOfBirth = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
                        simpleDateFormat.format(dateOfBirth);
                        violationLog.setDateOfBirth(dateOfBirth);
                    } else if (EditDateOfBirthField.getValue() != null || editableDate == null) {
                        Date date = violationLog.getDateOfBirth();
                    } else {
                        try {
                            String date = editableDate.getText();
                            dateOfBirth = new SimpleDateFormat("dd.MM.yyyy").parse(date);
                            violationLog.setDateOfBirth(dateOfBirth);
                        }catch (ParseException ex)
                        {
                            Date date=violationLog.getDateOfBirth();
                        }
                        }
                    if (EditIDField.getText().trim().isEmpty() || EditIDField == null) {
                        int id = violationLog.getId();
                    } else {
                        String inputID = EditIDField.getText();
                        int id = Integer.parseInt(inputID);
                        violationLog.setId(id);
                    }
                Date dateOfViolant=null;
                    TextField editableVioalantDate=EditDateOfVioalnt.getEditor();
                    if(EditDateOfVioalnt.getValue()!=null)
                    {
                        LocalDate localDate=EditDateOfVioalnt.getValue();
                        dateOfViolant=Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
                        simpleDateFormat.format(dateOfViolant);
                        violationLog.setDateOfBirth(dateOfViolant);
                    }
                    else if(EditDateOfVioalnt.getValue()!=null||editableVioalantDate==null)
                    {
                        Date date=violationLog.getDate();
                    }
                    else{
                        try {

                            String violantDate = editableVioalantDate.getText();
                            dateOfViolant = new SimpleDateFormat("dd.MM.yyyy").parse(violantDate);
                            violationLog.setDate(dateOfViolant);
                        }catch (ParseException ex)
                        {
                            Date date=violationLog.getDate();
                        }
                    }
                    if(EditTypeOFViolation.getValue()==null)
                    {
                        Fine fine=violationLog.getType();
                    }
                    else {
                        Fine fine=EditTypeOFViolation.getValue();
                        violationLog.setType(fine);
                    }
                    JOptionPane.showMessageDialog(null, "Изменения произошли успешно", "Success", JOptionPane.INFORMATION_MESSAGE);
                    mainMenuController.moveMainMenu(EditConfirmButton);
                }
                catch (IndexOutOfBoundsException ex) {
                JOptionPane.showMessageDialog(null,"Введите ФИО||Адрес полностью","Warning",JOptionPane.WARNING_MESSAGE);
                }
                catch (NumberFormatException ex)
                {
                JOptionPane.showMessageDialog(null,"ID должен быть целым число","Warning",JOptionPane.WARNING_MESSAGE);
                }
                EditInfoAboutViolatorController editInfoAboutViolatorController=new EditInfoAboutViolatorController();
                editInfoAboutViolatorController.saveInfo("ViolantionsLog.json",violationLogs);
            }
            else if (input==JOptionPane.CANCEL_OPTION)
            {
                mainMenuController.moveMainMenu(EditConfirmButton);
            }
        });
        }
    public void setIndex(int index)
    {
        index=index+1;
        String labelIndex=String.valueOf(index);
        IndexLabel.setText(labelIndex);
    }
    }

