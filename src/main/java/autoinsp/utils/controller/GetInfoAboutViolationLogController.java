package autoinsp.utils.controller;
import java.io.*;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import autoinsp.FucntionsApp.FunctionsApp;
import autoinsp.MainMenuController.MainMenuController;
import autoinsp.autoowners.model.Adress;
import autoinsp.autoowners.model.Fio;
import autoinsp.utils.model.Fine;
import autoinsp.utils.model.ViolationLog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javax.swing.*;

public class GetInfoAboutViolationLogController extends FunctionsApp implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView<ViolationLog> tableViolations;

    @FXML
    private TableColumn<ViolationLog, Integer> idColumn;

    @FXML
    private TableColumn<ViolationLog, Fio> FioColumnn;

    @FXML
    private TableColumn<ViolationLog, Date> DateOfBirthColumn;

    @FXML
    private TableColumn<ViolationLog, Adress> AdressColumn;

    @FXML
    private TableColumn<ViolationLog,String> PassColumn;

    @FXML
    private TableColumn<ViolationLog,Date> DateOfViolanceColumn;

    @FXML
    private TableColumn<ViolationLog, Fine> TypeOfViolance;

    @FXML
    private Button BackButton;

    @FXML
    private TextField SearchField;

    @FXML
    private Button RemoveSelectedPersonButton;

    @FXML
    private Button GetInfoAboutFinesButton;
    @FXML
    private Button GetSumOfFines;
    @FXML
    private Button EditSelectedViolator;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        idColumn.setCellValueFactory(new PropertyValueFactory<ViolationLog, Integer>("id"));
        FioColumnn.setCellValueFactory(new PropertyValueFactory<ViolationLog, Fio>("name"));
        DateOfBirthColumn.setCellValueFactory(new PropertyValueFactory<ViolationLog,Date>("dateOfBirth"));
        DateOfBirthColumn.setCellFactory(column -> {
            TableCell<ViolationLog, Date> cell = new TableCell<ViolationLog, Date>() {
                private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

                @Override
                protected void updateItem(Date item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                    } else {
                        setText(format.format(item));
                    }
                }
            };

            return cell;
        });
        AdressColumn.setCellValueFactory(new PropertyValueFactory<ViolationLog,Adress>("adress"));
        PassColumn.setCellValueFactory(new PropertyValueFactory<ViolationLog,String>("pass"));
        DateOfViolanceColumn.setCellValueFactory(new PropertyValueFactory<ViolationLog,Date>("date"));
        DateOfViolanceColumn.setCellFactory(column -> {
            TableCell<ViolationLog, Date> cell = new TableCell<ViolationLog, Date>() {
                private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

                @Override
                protected void updateItem(Date item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                    } else {
                        setText(format.format(item));
                    }
                }
            };

            return cell;
        });

        TypeOfViolance.setCellValueFactory(new PropertyValueFactory<ViolationLog, Fine>("type"));
        violationLogs=getViolationLogData();
        ObservableList<ViolationLog> violationsData = FXCollections.observableArrayList();
        violationsData.addAll(violationLogs);
        tableViolations.setItems(violationsData);
        FilteredList<ViolationLog>filteredListData=new FilteredList<>(violationsData,b->true);
        SearchField.textProperty().addListener((observable,oldvalue,newValue)->{
        filteredListData.setPredicate(violationLog -> {
    String space=" ";
    String firstname=violationLog.getName().getFirstName();
    String secondname=violationLog.getName().getSecondName();
    String lastname=violationLog.getName().getLastName();
    String Fio=firstname+space+secondname+space+lastname;
    String city=violationLog.getAdress().getCity();
    String street=violationLog.getAdress().getStreet();
    String zip=violationLog.getAdress().getZip();
    String adress=city+space+street+space+zip;
    DateFormat dateFormat=new SimpleDateFormat("dd.MM.yyyy");
    Date dateOfBirth=violationLog.getDateOfBirth();
    Date dateOfViolant=violationLog.getDate();
    String dateOfBirthString=dateFormat.format(dateOfBirth);
    String dateOfViolantString=dateFormat.format(dateOfViolant);
    if(newValue==null||newValue.isEmpty())
    {
        return true;
    }

    String lowerCaseFilter=newValue.toLowerCase();
    if((Fio.toLowerCase().indexOf(lowerCaseFilter))!=-1)
    {
        return true;
    }
    else if(adress.toLowerCase().indexOf(lowerCaseFilter)!=-1)
    {
        return true;
    }
    else if(violationLog.getPass().toLowerCase().indexOf(lowerCaseFilter)!=-1)
    {
        return true;
    }
    else if(String.valueOf(violationLog.getId()).indexOf(lowerCaseFilter)!=-1)
    {
        return true;
    }
    else if(dateOfBirthString.toLowerCase().indexOf(lowerCaseFilter)!=-1)
    {
        return true;
    }
    else if(dateOfViolantString.toLowerCase().indexOf(lowerCaseFilter)!=-1)
    {
        return true;
    }
    else if(violationLog.getType().getTypeOfFine().toLowerCase().indexOf(lowerCaseFilter)!=-1)
    {
        return true;
    }
    else
        return false;
});
        });
        SortedList<ViolationLog>sortedList=new SortedList<>(filteredListData);
        sortedList.comparatorProperty().bind(tableViolations.comparatorProperty());
        tableViolations.setItems(sortedList);
            BackButton.setOnAction(event -> {
            int input= JOptionPane.showConfirmDialog(null,"Вы действительно хотите вернуться в главное меню? ","Сделайте выбор",JOptionPane.YES_NO_OPTION);
            if(input==JOptionPane.YES_OPTION)
            {
                MainMenuController mainMenu=new MainMenuController();
               mainMenu.moveMainMenu(BackButton);
            }
        });
        GetInfoAboutFinesButton.setOnAction(event -> {
            String path="/view/GetInfoAboutFines.fxml";
            String title="Информация о штрафах";
            MainMenuController moveFines=new MainMenuController();
            moveFines.moveForm(GetInfoAboutFinesButton,path,title);
        });
        RemoveSelectedPersonButton.setOnAction(event -> {
            ViolationLog violationLog=tableViolations.getSelectionModel().getSelectedItem();
            int selectedIndex=violationLogs.indexOf(violationLog);
            if(selectedIndex==-1)
            {
                JOptionPane.showMessageDialog(null,"Вы ничего не выбрали","Warning",JOptionPane.WARNING_MESSAGE);
            }
            else {
                int input = JOptionPane.showConfirmDialog(null, "Вы действительно хотите удалить этого нарушителя из списка?", "Подтверждение выбора", JOptionPane.YES_NO_OPTION);
                if (input == JOptionPane.YES_OPTION) {
                    violationLogs.remove(selectedIndex);
                    tableViolations.setItems(violationsData);
                    tableViolations.getItems().remove(tableViolations.getSelectionModel().getSelectedItem());
                    JOptionPane.showMessageDialog(null, "Выбранный вами объект был успешно удален", "Success", JOptionPane.INFORMATION_MESSAGE);
                    GetInfoAboutViolationLogController getInfoAboutViolationLogController = new GetInfoAboutViolationLogController();
                    getInfoAboutViolationLogController.saveInfo("ViolantionsLog.json", violationLogs);
                }
            }
        });
        GetSumOfFines.setOnAction(event -> {
            ViolationLog violationLog=tableViolations.getSelectionModel().getSelectedItem();
            int selectedIndex=violationLogs.indexOf(violationLog);
            if(selectedIndex==-1)
            {
                JOptionPane.showMessageDialog(null,"Вы ничего не выбрали","Warning",JOptionPane.WARNING_MESSAGE);
            }
            else
            {
                Double sum= violationLog.getType().getSumOFFine();
                JOptionPane.showMessageDialog(null,"Размер штрафов выбранного автовладельца:"+sum+" руб.","Сумма штрафов",JOptionPane.INFORMATION_MESSAGE);
            }
        });
        EditSelectedViolator.setOnAction(event -> {
            int index=tableViolations.getSelectionModel().getSelectedIndex();
            if(index>-1)
            {
                tableViolations.setItems(violationsData);
                FXMLLoader loader=new FXMLLoader();
                EditSelectedViolator.getScene().getWindow().hide();
                loader.setLocation(getClass().getResource("/view/EditInfoAboutViolator.fxml"));
                try {
                    loader.load();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                Parent root=loader.getRoot();
                Stage stage=new Stage();
                stage.setScene(new Scene(root));
                Image ico = null;
                try {
                    ico = new Image(new FileInputStream("src/main/resources/images/icon.png"));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                EditInfoAboutViolatorController editInfoAboutViolatorController=loader.getController();
                editInfoAboutViolatorController.setIndex(index);
                stage.getIcons().add(ico);
                stage.setTitle("Редактирование информации о нарушителе");
                stage.show();
            }
            else {
                JOptionPane.showMessageDialog(null,"Вы ничего не выбрали ","Warning",JOptionPane.WARNING_MESSAGE);
            }
        });
    }
}
