package autoinsp.utils.controller;
import java.net.URL;
import java.util.*;
import autoinsp.FucntionsApp.FunctionsApp;
import autoinsp.MainMenuController.MainMenuController;
import autoinsp.utils.model.Fine;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.StringConverter;
import javax.swing.*;

public class GetInfoAboutFinesController extends FunctionsApp implements Initializable  {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView<Fine> tablefFines;

    @FXML
    private TableColumn<Fine,String> TypeOfFineColumn;

    @FXML
    private TableColumn<Fine, Double> SumOfFineColumn;

    @FXML
    private Button BackButton;

    @FXML
    private TextField SearchField;

    @FXML
    private TextField TypeViolantField;

    @FXML
    private TextField CostFineField;

    @FXML
    private Button AddNewFineButton;

    @FXML
    private Button RemoveFineButton;

    @FXML
    private Button EditFineButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        TypeOfFineColumn.setCellValueFactory(new PropertyValueFactory<Fine,String>("typeOfFine"));
        SumOfFineColumn.setCellValueFactory(new PropertyValueFactory<Fine, Double>("sumOFFine"));
        TypeOfFineColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        finesList=getFinesData();
        ObservableList<Fine>finesData=FXCollections.observableArrayList();
        finesData.addAll(finesList);
        tablefFines.setItems(finesData);
        tablefFines.setEditable(true);
        FilteredList<Fine>fineFilteredListData=new FilteredList<>(finesData,b->true);
        SearchField.textProperty().addListener((observable,oldvalue,newValue)->{
        fineFilteredListData.setPredicate(fine -> {
    if(newValue==null||newValue.isEmpty())
    {
        return true;
    }
    String lowerCaseFilter=newValue.toLowerCase();
if(fine.getTypeOfFine().toLowerCase().indexOf(lowerCaseFilter)!=-1)
{
    return true;
}
else if(String.valueOf(fine.getSumOFFine()).indexOf(lowerCaseFilter)!=-1)
{
    return true;
}
else
    return false;

});
        });
        SortedList<Fine>sortedList=new SortedList<>(fineFilteredListData);
        sortedList.comparatorProperty().bind(tablefFines.comparatorProperty());
        tablefFines.setItems(sortedList);
            SumOfFineColumn.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<Double>() {
            @Override
            public String toString(Double aDouble) {

                return aDouble ==null?"": aDouble.toString();
            }
            @Override
            public Double fromString(String s) {
                try {
                    return Double.valueOf(s);
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }));
        AddNewFineButton.setOnAction(event -> {
            int input = JOptionPane.showConfirmDialog(null,
                    "Вы готовы подтвердить введенные данные?", "Подтверждение данных", JOptionPane.YES_NO_OPTION);
            if (input ==JOptionPane.YES_OPTION)
            {
                try {
                    String typeOfViolant = TypeViolantField.getText();
                    String inputCost = CostFineField.getText();
                    Double costOfFine = Double.parseDouble(inputCost);
                    Fine fine = new Fine(typeOfViolant, costOfFine);
                    tablefFines.setItems(finesData);
                    tablefFines.getItems().add(fine);
                    FunctionsApp serialize=new FunctionsApp();
                    serialize.saveFinesToFile(fine);
                    JOptionPane.showMessageDialog(null,"Введенная вами информация былы успешно добавлена и сохранена","Success",JOptionPane.INFORMATION_MESSAGE);
                }
                catch (NumberFormatException ex)
                {
                    JOptionPane.showMessageDialog(null,"Размер штрафа должен быть числом!","Warning",JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        BackButton.setOnAction(event -> {
            int input= JOptionPane.showConfirmDialog(null,"Вы действительно хотите вернуться в главное? ","Сделайте выбор",JOptionPane.YES_NO_OPTION);
            if(input==JOptionPane.YES_OPTION)
            {
                MainMenuController mainMenuController=new MainMenuController();
                mainMenuController.moveMainMenu(BackButton);
            }
        });
        RemoveFineButton.setOnAction(event -> {
            Fine fine = tablefFines.getSelectionModel().getSelectedItem();
            tablefFines.setItems(finesData);
            int selectedIndex = finesList.indexOf(fine);
            if (selectedIndex == -1) {
                JOptionPane.showMessageDialog(null, "Вы ничего не выбрали", "Warning", JOptionPane.WARNING_MESSAGE);
            }
            else {
                int input = JOptionPane.showConfirmDialog(null,
                        "Вы действительно хотите удалить выбранное вами нарушение?", "Подтверждение выбора", JOptionPane.YES_NO_OPTION);
                if (input == JOptionPane.YES_OPTION) {
                    finesList.remove(selectedIndex);
                    tablefFines.getItems().remove(tablefFines.getSelectionModel().getSelectedItem());
                    JOptionPane.showMessageDialog(null, "Выбранный вами объект был успешно удален", "Success", JOptionPane.INFORMATION_MESSAGE);
                    GetInfoAboutFinesController getInfoAboutFinesController=new GetInfoAboutFinesController();
                    getInfoAboutFinesController.saveInfo("Fines.json",finesList);
                    }
                }
        });
        EditFineButton.setOnAction(event -> {
            int input = JOptionPane.showConfirmDialog(null,
                    "Вы готовы сохранить изменения?", "Подтверждение изменений", JOptionPane.YES_NO_OPTION);
            if(input==JOptionPane.YES_OPTION) {
             GetInfoAboutFinesController getInfoAboutFinesController=new GetInfoAboutFinesController();
             String path="Fines.json";
             getInfoAboutFinesController.saveInfo(path,finesList);
                JOptionPane.showMessageDialog(null, "Измененя были успешно сохранены", "Success", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        }
    public void ChangeTypeOfVioaltionCommit(TableColumn.CellEditEvent event) {
        Fine fine=tablefFines.getSelectionModel().getSelectedItem();
        fine.setTypeOfFine(event.getNewValue().toString());
    }
    public void GetEditSumCommit(TableColumn.CellEditEvent event) {
        Fine fine = tablefFines.getSelectionModel().getSelectedItem();
        fine.setSumOFFine(Double.valueOf(event.getNewValue().toString()));
    }
    }

