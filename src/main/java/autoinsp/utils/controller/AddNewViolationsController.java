package autoinsp.utils.controller;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
import autoinsp.FucntionsApp.FunctionsApp;
import autoinsp.MainMenuController.MainMenuController;
import autoinsp.autoowners.model.Adress;
import autoinsp.autoowners.model.Fio;
import autoinsp.utils.model.Fine;
import autoinsp.utils.model.ViolationLog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javax.swing.*;
public class AddNewViolationsController extends FunctionsApp implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private DatePicker DateOfViolantsPicker;

    @FXML
    private TextField FIOField;

    @FXML
    private DatePicker DateOfBirthField;

    @FXML
    private TextField PassField;

    @FXML
    private TextField AdressField;

    @FXML
    private TextField IDField;

    @FXML
    private Button ConfirmButton;
    @FXML
    private ComboBox<Fine> ComboBoxType;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        finesList=getFinesData();
        ObservableList<Fine> finesDataList= FXCollections.observableArrayList();
        finesDataList.addAll(finesList);
        ComboBoxType.setItems(finesDataList);
        ConfirmButton.setOnAction(event -> {
            int input = JOptionPane.showConfirmDialog(null,
                    "Вы готовы подтвердить данные?", "Подтверждение данных", JOptionPane.YES_NO_CANCEL_OPTION);
            MainMenuController mainMenuController=new MainMenuController();
            if (input ==JOptionPane.YES_OPTION) {
                try {
                    String inputName = FIOField.getText();
                    Fio fio = new Fio();
                    String splitName[] = inputName.split("\\s");
                    fio.setFirstName(splitName[0]);
                    fio.setSecondName(splitName[1]);
                    fio.setLastName(splitName[2]);
                    TextField editableDate = DateOfBirthField.getEditor();
                    String date = editableDate.getText();
                    Date dateOfBirth = null;
                    dateOfBirth = new SimpleDateFormat("dd.mm.yyyy").parse(date);
                    if (DateOfBirthField.getValue() != null) {
                        LocalDate localDate = DateOfBirthField.getValue();
                        dateOfBirth = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.mm.yyyy");
                        simpleDateFormat.format(dateOfBirth);
                    }
                    Adress adress = new Adress();
                    String inputAdress = AdressField.getText();
                    String[] splitter = inputAdress.split("\\s");
                    adress.setCity(splitter[0]);
                    adress.setStreet(splitter[1]);
                    adress.setZip(splitter[2]);
                    String pass = PassField.getText();
                    String inputId = IDField.getText();
                    int id = Integer.parseInt(inputId);
                    Fine fine=new Fine();
                    fine=ComboBoxType.getValue();
                    TextField editableDateOfViolant=DateOfViolantsPicker.getEditor();
                    String editableViolatString=editableDateOfViolant.getText();
                    Date dateOfViolant=null;
                    dateOfViolant=new SimpleDateFormat("dd.mm.yyyy").parse(editableViolatString);
                if(DateOfViolantsPicker.getValue()!=null)
                {
                    LocalDate localDate=DateOfViolantsPicker.getValue();
                    dateOfViolant=Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                    SimpleDateFormat simpleDateFormat=new SimpleDateFormat(("dd.mm.yyyy"));
                    simpleDateFormat.format(dateOfViolant);
                }
                    ViolationLog violationLog=new ViolationLog(fio,dateOfBirth,adress,pass,id,dateOfViolant,fine);
                FunctionsApp serialize=new FunctionsApp();
                serialize.saveToFileViolantionLog(violationLog);
                    JOptionPane.showMessageDialog(null,"Добавленные данные нарушителя :"+violationLog,"Нарушения",JOptionPane.INFORMATION_MESSAGE);
                    mainMenuController.moveMainMenu(ConfirmButton);
                }
                catch (IndexOutOfBoundsException ex)
                {
                    JOptionPane.showMessageDialog(null,"Введите ФИО/адрес полностью","Warning",JOptionPane.WARNING_MESSAGE);
                }
                catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Id должен быть целочисленным значением", "Error", JOptionPane.ERROR_MESSAGE);
                }
                     catch (ParseException ex)
                    {
                        JOptionPane.showMessageDialog(null,"Введите дату в указанном формате","Warning",JOptionPane.WARNING_MESSAGE);
                    }
                }
            else if(input==JOptionPane.CANCEL_OPTION)
            {
                mainMenuController.moveMainMenu(ConfirmButton);
            }
    });
}
}

